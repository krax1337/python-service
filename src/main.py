
import socket
from geopy.geocoders import Nominatim

geolocator = Nominatim(user_agent="dsa", timeout=10000)

hostname = socket.gethostname()
ip_address = socket.gethostbyname(hostname)
city = "Тараз"

location = geolocator.geocode(city)
print(location)

# Define socket host and port
SERVER_HOST = '0.0.0.0'
SERVER_PORT = 8080

# Create socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind((SERVER_HOST, SERVER_PORT))
server_socket.listen(1)
print('Listening on port %s ...' % SERVER_PORT)

while True:    
    # Wait for client connections
    client_connection, client_address = server_socket.accept()

    # Get the client request
    request = client_connection.recv(1024).decode()
    print(request)

    # Send HTTP response
    response = f'HTTP/1.0 200 OK\n\nHello Docker v2 {hostname} {ip_address}'
    print(location)


    client_connection.sendall(response.encode())
    client_connection.close()

# Close socket
server_socket.close()