FROM python:alpine

WORKDIR /app

ENV SUPER_API=ABC

COPY src/requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

EXPOSE 8080

COPY src/ .

ENTRYPOINT [ "python3" ]

CMD [ "main.py" ]